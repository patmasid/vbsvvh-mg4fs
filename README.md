# VBSVVh-MG4fs

This repo is generated using the repos by Karolos <karolos.potamianons@cern.ch> (https://gitlab.cern.ch/atlas-physics/HDBS/DBL/ana-hdbs-2023-26/vbshiggs/signalproduction/vvhjj-mg) and Viviana <viviana.cavaliere@cern.ch> (https://gitlab.cern.ch/vcavalie/vvhjj_semileptonic_mg_gen).

Contact for this repo: Prachi Atmasiddha <prachi.atmasiddha@cern.ch>.

## Cloning the git repo: 

```
git clone ssh://git@gitlab.cern.ch:7999/patmasid/vbsvvh-mg4fs.git
cd vbsvvh-mg4fs
workdir=$PWD
sed -i "s|MY_WORK_DIR|${workdir}|g" EventGeneration/VVHjjSemilep/ssWWhjj/2000*/*.py
sed -i "s|MY_WORK_DIR|${workdir}|g" GridPackGeneration/VVHjjSemilep/ssWWhjj/2000*/*.py
sed -i "s|MY_WORK_DIR|${workdir}|g" EventGeneration/VVHjjSemilep/ssWWhjj/*.sh
sed -i "s|MY_WORK_DIR|${workdir}|g" GridPackGeneration/VVHjjSemilep/ssWWhjj/*.sh
sed -i "s|MY_WORK_DIR|${workdir}|g" EventGeneration/VVHjjSemilep/ssWWhjj/condor/submit*
sed -i "s|MY_WORK_DIR|${workdir}|g" GridPackGeneration/VVHjjSemilep/ssWWhjj/condor/submit*
```

## Job Options to generate VVHjj samples in semileptonic VBS ssWWh.

The model used is the official one at ```/cvmfs/atlas.cern.ch/repo/sw/Generators/madgraph/models/latest/HHVBF_UFO```. The ```restrict_5FSwithYukawa.dat``` file is used to derive ```restrict_4FS.dat``` and ```restrict_4FSZeroYukawa.dat```. The common change to get both of these is to add the bottom quark mass in the ```Block MASS```. In ```4FSZeroYukawa``` file, along with adding bottom quark mass, the ```Block YUKAWA``` is also changed. The yukawa couplings of the ```d u s c``` quarks are set to 0. This speeds up the generation process.

To use these models in the JOs:

To use ```restrict_4FS.dat```:
```
import model MY_DATA_DIR/models/HHVBF_UFO-4FS
```

To use ```restrict_4FSZeroYukawa.dat```:
```
import model MY_DATA_DIR/models/HHVBF_UFO-4FSZeroYukawa
```

The number of events ```maxEvents``` is hardcoded inside the JO as of now. This can be changed. Currently generating 10000 events is giving an issue so we are generating only 2000 per file.

The DSIDs and the corresponsing ```kl``` value:

```
200000: kl=+1
200001: kl=+2
200002: kl=+5
200003: kl=10
200004: kl=-2
200005: kl=-5
200006: kl=-10
200007: kl=+20
200008:	kl=-20
200009: kl=0
200010: kl=-1
```

## Generating gridpacks
To generate gridpacks, one should use the makeGridPack_Singularity.sh script (or derive their own from it).
Usage for this script is:

```./GridPackGeneration/makeGridPack_Singularity.sh {DSID} {BaseDir} {NCores}```
For example:

```./GridPackGeneration/makeGridPack_Singularity.sh 200001 $PWD 8```
You might need to edit the script to your local setup but basically the script does the following

Create a new directory (eventually in a scratch space): ```MkMGGridPack-${DSID}```

Copy the DSID folder and possibly some .f, .dat, .py and other files that modify Madgraph's behaviour
Setup the proper AthGeneration release
Run the command ```Gen_tf.py --ecmEnergy=13000. --maxEvents=-1 --firstEvent=1 --randomSeed=123456 --outputTXTFile=fake_lhe_events --jobConfig=${DSID} --outputFileValidation=False (with the proper parallelisation as set by [NCores]```

Copy the output file to a subfolder of ${BaseDir} with the same name as in the 1st step. Note that it doesn't clear the working directory (this is usually done by Condor when the job is exitted).


## Running using previously generated gridpack (or without making a gridpack)
To run from gridpacks, one should use the makeEVNTfromGridPack_Singularity.sh script (or derive their own from it).
Usage for this script is:

```./EventGeneration/makeEVNTfromGridPack_Singularity.sh {DSID} {BaseDir} {nCores} {RunNo}```
For example:

```./EventGeneration/makeEVNTfromGridPack_Singularity.sh 200001 $PWD 8 1```
You might need to edit the script to your local setup but basically the script does the following

Create a new directory (eventually in a scratch space): ```MkEVNTfromGridPack-${DSID}-Run${RunNo}```

Copy the DSID folder and possibly some .f, .dat, .py and other files that modify Madgraph's behaviour
Setup the proper AthGeneration release
Run the command ```Gen_tf.py --ecmEnergy=13000. --maxEvents=${maxEvents} --firstEvent=${firstEvent} --randomSeed=${randomSeed} --outputTXTFile="OUT.TXT._${fileNo}.tar.gz.1" --jobConfig=${DSID} --outputEVNTFile="EVNT.${DSID}._${fileNo}.pool.root.1"```

Copy the output file to a subfolder of ```${BaseDir}``` with the same name as in the 1st step. Note that it doesn't clear the working directory (this is usually done by Condor when the job is exitted).