#!/usr/bin/env bash

jobDir=MY_DATA_DIR/GridPackGeneration/VVHjjSemilep/ssWWhjj

ATLAS_RELEASE=22.6.23
ASETUP="asetup"

# Running with Singularity
isCentos7=$(uname -r | grep -c '\.el7\.')
if [[ ${isCentos7} -eq 0 && "$1" != "--really" ]]; then
  echo "Running with singularity..."
  exec singularity exec -e --no-home -B /scratch -B /cvmfs/atlas.cern.ch -B /cvmfs/atlas-nightlies.cern.ch -B /cvmfs/atlas-condb.cern.ch -B /cvmfs/sft.cern.ch -B /cvmfs/unpacked.cern.ch -B /cvmfs/sft-nightlies.cern.ch -B /var -B $PWD /cvmfs/atlas.cern.ch/repo/containers/fs/singularity/x86_64-centos7 /bin/bash -- "$0" --really "$@"
  exit
fi
[ "$1" == "--really" ] && shift

[ -z ${3} ] && echo ${0} DSID baseDir nCpus RunNo && exit
echo ${0}: $@

DSID=${1}
baseDir=${2}
nCpus=${3:-12}
RunNo=${4}

#echo Ensuring presence of directory ${jobDir}
#mkdir -p ${jobDir}

cd ${baseDir}
baseDir=$( readlink -f ${PWD} )

runDir=${baseDir}/MkEVNTfromGridPack-${DSID}-Run${RunNo}

COPY_RESULTS=0
if [ ${COPY_RESULTS} -eq 1 ] ; then
  wouldbeRunDir=${runDir}
  mkdir -p ${wouldbeRunDir}
  runDir=${jobDir}/MkEVNTfromGridPack-${DSID}-Run${RunNo}
  mkdir -p ${runDir}
fi
echo Running in directory ${runDir}

mkdir ${runDir}
rsync -aP *.py *.dat *.f ${runDir}
rsync -aP --copy-links ${DSID} ${runDir}

cd ${runDir}

export ATHENA_PROC_NUMBER=${nCpus}

echo ATHENA_PROC_NUMBER=${ATHENA_PROC_NUMBER}

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh

${ASETUP} ${ATLAS_RELEASE},AthGeneration

export MAKEFLAGS="QUICK=1"
export FFLAGS="-w -fPIC -mcmodel=medium"

maxEvents=$((2000))
firstEvent=$((${RunNo} * ${maxEvents} + 1))

${PYTHON} Gen_tf.py --ecmEnergy=13000. --maxEvents=${maxEvents} --firstEvent=${firstEvent} --randomSeed=10${RunNo}00 --outputTXTFile="OUT.TXT._${RunNo}.tar.gz.1" --jobConfig=${DSID} --outputEVNTFile="EVNT.${DSID}._${RunNo}.pool.root.1"

[ ${COPY_RESULTS} -eq 1 ] && echo "Copying output to ${wouldbeRunDir} ..." && rsync -aP log.generate mc*.tar.gz ${wouldbeRunDir}

# Check that the sync worked, and delete the folder

[ ${COPY_RESULTS} -eq 1 ] && [ "$(md5sum ${wouldbeRunDir}/mc.*tar.gz | awk '{ print $1 }')" == "$(md5sum mc*.tar.gz | awk '{ print $1 }')" ] && cd - #&& rm -rf ${runDir}
