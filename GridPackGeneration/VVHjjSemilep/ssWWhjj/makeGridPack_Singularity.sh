#!/usr/bin/env bash

jobDir=MY_DATA_DIR/GridPackGeneration/VVHjjSemilep/ssWWhjj

ATLAS_RELEASE=22.6.23
ASETUP="asetup"

# Running with Singularity
isCentos7=$(uname -r | grep -c '\.el7\.')
if [[ ${isCentos7} -eq 0 && "$1" != "--really" ]]; then
  echo "Running with singularity..."
  exec singularity exec -e --no-home -B /scratch -B /cvmfs/atlas.cern.ch -B /cvmfs/atlas-nightlies.cern.ch -B /cvmfs/atlas-condb.cern.ch -B /cvmfs/sft.cern.ch -B /cvmfs/unpacked.cern.ch -B /cvmfs/sft-nightlies.cern.ch -B /var -B $PWD /cvmfs/atlas.cern.ch/repo/containers/fs/singularity/x86_64-centos7 /bin/bash -- "$0" --really "$@"
  exit
fi
[ "$1" == "--really" ] && shift

[ -z ${4} ] && echo ${0} DSID baseDir nCpus && exit
echo ${0}: $@

DSID=${1}
baseDir=${2}
nCpus=${3:-12}

#echo Ensuring presence of directory ${jobDir}
#mkdir -p ${jobDir}

cd ${baseDir}
baseDir=$( readlink -f ${PWD} )

runDir=${baseDir}/MkMGGridPack-${DSID}

COPY_RESULTS=0
if [ ${COPY_RESULTS} -eq 1 ] ; then
  wouldbeRunDir=${runDir}
  mkdir -p ${wouldbeRunDir}
  runDir=${jobDir}/MkMGGridPack-${DSID}
  mkdir -p ${runDir}
fi
echo Running in directory ${runDir}

mkdir ${runDir}
rsync -aP *.py *.dat *.f ${runDir}
rsync -aP --copy-links ${DSID} ${runDir}

cd ${runDir}

export ATHENA_PROC_NUMBER=${nCpus}

echo ATHENA_PROC_NUMBER=${ATHENA_PROC_NUMBER}

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh

${ASETUP} ${ATLAS_RELEASE},AthGeneration

export MAKEFLAGS="QUICK=1"
export FFLAGS="-w -fPIC -mcmodel=medium"
${PYTHON} Gen_tf.py --ecmEnergy=13000. --maxEvents=-1 --firstEvent=1 --randomSeed=123456 --outputTXTFile=fake_lhe_events --jobConfig=${DSID} --outputFileValidation=False

#Generate_tf.py --outputEVNTFile=%OUT.EVNT.pool.root --ecmEnergy=13000 --runNumber=${DSID} --jobConfig=${joFile} --maxEvents 100 --randomSeed 1 --outputTXTFile=%OUT.TXT._0001.tar.gz.1

[ ${COPY_RESULTS} -eq 1 ] && echo "Copying output to ${wouldbeRunDir} ..." && rsync -aP log.generate mc*.tar.gz ${wouldbeRunDir}

# Check that the sync worked, and delete the folder

[ ${COPY_RESULTS} -eq 1 ] && [ "$(md5sum ${wouldbeRunDir}/mc.*tar.gz | awk '{ print $1 }')" == "$(md5sum mc*.tar.gz | awk '{ print $1 }')" ] && cd - #&& rm -rf ${runDir}

