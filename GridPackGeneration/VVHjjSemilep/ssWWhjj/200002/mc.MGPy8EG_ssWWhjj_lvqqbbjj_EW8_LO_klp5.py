# using HHVBF UFO

from MadGraphControl.MadGraphUtils import *

# ----------------------------------------------------------------------------
# Adding block for PDF and other variations
# ----------------------------------------------------------------------------
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING["alternative_dynamic_scales"] =  [1,2,3,4]

# ----------------------------------------------------------------------------
# Define physics process to be simulated
# ----------------------------------------------------------------------------

mgproc = """
generate p p > w+ w+ h j j QCD=0, w+ > l+ vl, w+ > j j QCD=0  @0
add process p p > w- w- h j j QCD=0, w- > l- vl~, w- > j j QCD=0 @0
"""
runName = 'ssWWhjj_LO_hhvbf_lvqqbb'


gridpack_mode = True
gridpack_compile = True
savePROCdir = True # true only for testing

MADGRAPH_CATCH_ERRORS=False

physShort = get_physics_short()

#---------------------------------------------------------------------------------------------------
# Number of Events
# The number of generated events are set to safefactor times maxEvents,
# to avoid crashing due to not having enough events
# Also putting protection to avoid from crashing when maxEvents=-1
#---------------------------------------------------------------------------------------------------
safefactor = 1.1
evgenConfig.nEventsPerJob = 10000
nevents = safefactor * evgenConfig.nEventsPerJob  


#---------------------------------------------------------------------------------------------------
# Setting parameters for param_card.dat; CX=1 are the SM parameters
#---------------------------------------------------------------------------------------------------
parameters = {}
parameters['NEW'] = {'CV':  '1.0000000',  # CV
                     'C2V': '1.0000000',  # C2V
                     'C3':  int(physShort[physShort.find("_kl")+3:].translate(str.maketrans('pm','+-')))}  # C3, a.k.a kl

#---------------------------------------------------------------------------------------------------
# Setting higgs mass to 125 GeV for param_card.dat
#---------------------------------------------------------------------------------------------------
parameters['MASS']={'25':'1.250000e+02'} #MH

#---------------------------------------------------------------------------------------------------
# Setting some parameters for run_card.dat
#---------------------------------------------------------------------------------------------------

extras = { 'lhe_version':'3.0',
           'ptj': 10.0,
           'drjj': 0.2,
           'etaj': 5.5,
           'auto_ptj_mjj':False,
           'cut_decays': False,
           'systematics_program': 'systematics',
         }


extras["nevents"] = int(nevents)


#---------------------------------------------------------------------------------------------------
# Generating VBS like process plus H with MadGraph
# Parameters are set above
#---------------------------------------------------------------------------------------------------

if not is_gen_from_gridpack():

  process="""
  import model MY_DATA_DIR/models/HHVBF_UFO-4FSZeroYukawa
  define p = g u c d s u~ c~ d~ s~ b b~
  define j = g u c d s u~ c~ d~ s~ b b~
  define l+ = e+ mu+
  define l- = e- mu-
  define vl = ve vm
  define vl~ = ve~ vm~
  """ + mgproc + """
  output -f"""

  process_dir = new_process(process)

else:
  process_dir = MADGRAPH_GRIDPACK_LOCATION

#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the run_card
# https://gitlab.cern.ch/atlas/athena/-/tree/master/Generators/MadGraphControl
# Build a new run_card.dat from an existing one
# Using the values given in "extras" above for the selected parameters when setting up the run_card
# If not set in "extras", default values are used 
#---------------------------------------------------------------------------------------------------
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

#----------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the param_card
# https://gitlab.cern.ch/atlas/athena/-/tree/master/Generators/MadGraphControl
# Build a new param_card.dat from an existing one
# Used values given in "parameters" for CV, C2V, C3
# Higgs mass is set to 125 GeV by "higgsMass"
#---------------------------------------------------------------------------------------------------
modify_param_card(process_dir=process_dir,params=parameters)

#---------------------------------------------------------------------------------------------------
# Printing cards   
#---------------------------------------------------------------------------------------------------
print_cards()

#---------------------------------------------------------------------------------------------------
# Generate events                                                               
#---------------------------------------------------------------------------------------------------     
generate(process_dir=process_dir,runArgs=runArgs, grid_pack=gridpack_mode,gridpack_compile=gridpack_compile)

#---------------------------------------------------------------------------------------------------
# Move output files into the appropriate place, with the appropriate name
#---------------------------------------------------------------------------------------------------   
outputDS = arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=savePROCdir)

# Helper for resetting process number; multicore only used for gridpack generation
check_reset_proc_number(opts)

#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators = ["MadGraph", "Pythia8"]
evgenConfig.description = "Higgs production through ssWW scattering with Higgs decaying to bb and the Ws semi-leptonically (e,mu) in HHHVBF_UFO."
evgenConfig.keywords = ["Higgs", "SM", "SMHiggs", "VBS", "electroweak", "bottom"]
evgenConfig.contact = ['Lisa Marie Lehmann  <Lisa.Marie.Lehmann@cern.ch>','Karolos Potamianos <karolos.potamianos@cern.ch>']

runArgs.inputGeneratorFile=outputDS
#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")

genSeq.Pythia8.Commands += ["25:onMode = off",
                            "25:oneChannel = 1 1 100 5 -5 "]  # bb decay

#--------------------------------------------------------------                                                                                   
# Dipole option Pythia8                                                                                                          
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ "SpaceShower:dipoleRecoil = on" ]

